from aplicacion import *
from conexion import *
from received import *
import queue

enviar = queue.Queue()
recibir = queue.Queue()
comandos = queue.Queue()

def main():
	conexion = Conexion(enviar, recibir, comandos)
	conexion.start()

	recibido = Received(enviar, recibir)
	recibido.start()

	continua = input("")

	app = Aplicacion(enviar, recibir, comandos)
	app.Lectura()

	return 0


if __name__ == '__main__':
	main()
