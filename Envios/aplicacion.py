from tkinter import *
from envios import *
import queue
import time

class Aplicacion(object):
	def __init__(self, enviar, recibir, comandos):

		self.enviar = enviar
		self.recibir = recibir
		self.comandos = comandos

	def Lectura(self):
		archivo = open('comandos.txt','r')

		while True:
			linea = archivo.readline()
			if not linea:
				break

			tam = len(linea)
			posicion = 2
			Palabra = ""

			while posicion < tam:
				ch = linea[posicion]
				posicion = posicion + 1
        
				if ch != ",":
					Palabra = Palabra + ch

				else:
					self.comandos.put(Palabra)
					Palabra = ""
					posicion = posicion + 1
			time.sleep(3)
			
			app2 = Envios(self.enviar, self.recibir, self.comandos)
			app2.Envio()
