import socket
import threading

class Conexion(threading.Thread):

	def __init__(self, enviar, recibir, comandos):
		threading.Thread.__init__(self)
		self.enviar = enviar
		self.recibir = recibir
		self.comandos = comandos

	def run(self):
		host = '169.254.197.165'
		port = 4444

		client = socket.socket()
		client.connect((host,port))

		while True:
			try:
				data = client.recv(1024).decode()
				self.recibir.put(data)
				print ('Received: ' + data)

				x = self.enviar.get()
				if "" != x:
					print("SEND:", x)
					x = x + '\n'
					client.send(x.encode('utf-8'))
					if "bye\n" == x:
						print("Shutting down.")
						break

			except KeyboardInterrupt as k:
				print("Shutting down.")
				client.close()
				break
