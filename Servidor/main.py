from newClient import *

def main():

	host = "192.168.1.70"
	port = 4444

	mySocket = socket.socket()

	clients = set()
	clients_lock = threading.Lock()

	mySocket.bind((host, port))
	mySocket.listen(10)

	while True:
		conn, addr = mySocket.accept()

		nuevoCliente = newClient(conn, addr, clients, clients_lock)
		nuevoCliente.start()

		print("Dispositivo conectado: " + str(addr))


	return 0;

if __name__ == '__main__':
	main()
