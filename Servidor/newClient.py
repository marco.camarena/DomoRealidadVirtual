import socket
import threading
import time

class newClient(threading.Thread):

	def __init__(self, conn, addr, clients, clients_lock):
		threading.Thread.__init__(self)
		self.conn = conn
		self.addr = addr
		self.clients = clients
		self.clients_lock = clients_lock
		self.nombre = ""

	def run(self):

		with self.clients_lock:
			self.clients.add(self.conn)

		while True:

			data = self.conn.recv(1024).decode()
			
			if not data:
				return 0

			#SE CAMBIA EL MENSAJE RECIBIDO A MAYUSCULAS
			data = str(data).upper()

			#SE GUARDA EL NOMBRE DEL DISPOSITIVO
			if self.nombre == "":
				self.nombre = data

			if data == "EXIT\n":
				break

			#SE IMPRIME EL MENSAJE RECIBIDO Y SE LE AGREGA UN SALTO
			print("Recibido: " + str(data))
			data = data + "\n"

			#SE ENVIA EL MENSAJE A TODOS LOS CLIENTES
			with self.clients_lock:
				for c in self.clients:
					try:
						c.send(data.encode())

					except BrokenPipeError:
						#print("No se pudo enviar a: ")
						#c.close()
						#self.clients.remove(c)
		print("CERRANDO SERVIDOR")
		self.conn.close()					
#self.conn.send(data.encode())
