#include <SPI.h>
#include <Ethernet.h>

byte mac[] = {  0x98, 0x4F, 0xEE, 0x00, 0xE5, 0x1D }; //REVISAR MAC DE LA TARJETA

IPAddress server(192, 168, 1, 222);

IPAddress direccion_ip_fija(192, 168, 1, 20); // Dirección IP de Galileo

IPAddress puerta_enlace(192, 168, 1, 1);
IPAddress mascara_red(255, 255, 255, 0);

EthernetClient client;

void setup() {
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  Serial.begin(9600);

  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  Ethernet.begin(mac, direccion_ip_fija, puerta_enlace, mascara_red);

  // give the Ethernet shield a second to initialize:
  delay(1000);
  Serial.println("connecting...");

  // if you get a connection, report back via serial:
  if (client.connect(server, 4444)) {
    Serial.println("connected");
    // Make a HTTP request:
    //client.println("Hola soy Marco");
  } else {
    // if you didn't get a connection to the server:
    Serial.println("connection failed");
  }

  client.print("VENTILADOR 1");
}

void loop() {
  String ipCadena = "";
  String cadena = "";
  String action = "";
  String Sled = "13";
  int led = 13;

  while (true) {
    if (client.available()) {
      char c = client.read();
      if (c == '\n') {
        break;
      }
      cadena.concat(c);
    }
  }

  Serial.print("Recibido: ");
  Serial.println(cadena);

  if(cadena == "DD"){
    client.print("DIS 192.168.1.20 VENTILADOR 1");
  }else{
    ipCadena = cadena.substring(6, 18);
     
    if(ipCadena == "192.168.1.20"){
      
      action = cadena.substring(28);
          
      if (action == "H") {
        Sled = cadena.substring(22, 24);
        led = Sled.toInt();
        digitalWrite(led, HIGH);
      } else if (action == "L") {
        Sled = cadena.substring(22, 24);
        led = Sled.toInt();
        digitalWrite(led, LOW);
      }
    }
  }
  // if the server's disconnected, stop the client:
  if (!client.connected()) {
    Serial.println();
    Serial.println("disconnecting.");
    client.stop();

    // do nothing forevermore:
    for (;;)
      ;
  }
}
